<?php

/**
 * @file
 * This file has the plugin defined for one to one relationship.
 */

$plugin = array(
  'title' => t('Validate one to one entities'),
  'description' => t('Provides special validation for Entity Reference field.
    An validation error will be shown if entity is added already.'),
  'class' => 'EntityreferenceOneToOneRelationship',
  'behavior type' => 'field',
);
