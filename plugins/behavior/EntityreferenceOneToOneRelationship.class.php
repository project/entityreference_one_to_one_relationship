<?php

/**
 * @file
 * Entityreference behavior validation plugin.
 *
 * This file has the Entityreference behavior validation plugin
 * defined for one to one relationship.
 */

/**
 * The Controller for Entity reference one to one behavior.
 */
class EntityreferenceOneToOneRelationship extends EntityReference_BehaviorHandler_Abstract {

  /**
   * Provide a validation for one to one relationship.
   */
  public function validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
    $items_value = array();
    // Prepare items values in array.
    foreach ($items as $item) {
      if ($item['target_id']) {
        $items_value[] = $item['target_id'];
      }
    }

    if (empty($items_value)) {
      return;
    }

    $count_entities = _entityreference_one_to_one_relationship_get_reference_count($field, $items_value, $entity->type);

    if ($count_entities >= 1) {
      // Check in current node for existing record.
      $check_for_existing = _entityreference_one_to_one_relationship_check_for_existing_entity($field, $items_value, $entity->nid, $entity->type);

      // Get field instance settings to get field label.
      $field_instance = field_info_instance($entity_type, $field['field_name'], $entity->type);
      $reference_type = implode(', ', $field['settings']['handler_settings']['target_bundles']);

      // Prepare error message.
      $error = t('%name field can only have one to one relationship, this %reference_type reference is already assigned to different %entity_type.', array(
        '%name' => $field_instance['label'],
        '%entity_type' => $entity->type,
        '%reference_type' => $reference_type,
      ));
      if (!isset($entity->nid)) {
        form_set_error($field['field_name'], $error);
      }
      elseif ($check_for_existing != $entity->nid) {
        form_set_error($field['field_name'], $error);
      }
    }
  }

}
