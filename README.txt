This module is for validating one to one relationship with entity reference
fields. For example: If you create a entity reference field say
<strong>Article</strong> which is referencing content from
<strong>Basic page</strong> and you have added a new Article referencing Basic
page's content say "Test A". Then if you try to add a new article referencing
the same content from Basic page which was "Test A" then it will throw and
exception saying that "Your field name can only have one to one relationship,
this basic page reference is already assigned to different article".

## How to configure:
<ul>
  <li>Go to your Entity reference field settings page</li>
  <li>Check "Validate one to one entities" in "Additional behaviors"
  section</li>
  <li>You are good to go. Play around!</li>
</ul>

## Known issue:
<ul>
  <li>Does not working with multi valued field selection</li>
</ul>

## Highlights:
<ul>
  <li>Works with all type of widgets</li>
  <li>Specific to respective content types</li>
</ul>

D8 Module Port: Already in progress.
